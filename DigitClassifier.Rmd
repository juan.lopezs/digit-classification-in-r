---
title: "Digit Classifier"
author: "Juan López Salar"
output: 
  html_document:
  number_sections: yes
theme: spacelab
toc: yes
toc_float: yes
editor_options: 
  chunk_output_type: console
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r,eval=FALSE, echo=FALSE}
install.packages("lattice")
install.packages("https://cran.r-project.org/src/contrib/Archive/nutshell.bbdb/nutshell.bbdb_1.0.tar.gz", 
                 repos = NULL, type="source")
install.packages("https://cran.r-project.org/src/contrib/Archive/nutshell.audioscrobbler/nutshell.audioscrobbler_1.0.tar.gz", 
                 repos = NULL, type="source")
install.packages("https://cran.r-project.org/src/contrib/Archive/nutshell/nutshell_2.0.tar.gz", 
                 repos = NULL, type="source")
install.packages("gridExtra")
```

# Introduction

## Reading the data
The first step is to read the data. The training and the test sets are contained in their respective ones:
  
```{r}
# It's important to set the working directory where we have the necessary files to compile this file
path = "/home/juanxi/Desktop/git/digit-classification-in-r/"
setwd(path)

# Read csvs
train = read.csv("mnist_test.csv", header=F)
test = read.csv("mnist_train.csv", header=F)


# As we are in a classification quest, the result column must be saved as a factor
train.result = as.factor(train[,1])
test.result = as.factor(test[,1])

# Setting a seed before doing our experiments
set.seed(1234)
```

## Data preparation before PCA
First of all, it is obligatory to drop every column having variance near 0. Those columns doesn't give us any information, and only will make our calculations slower. Also, as PCA is an unsupervised tecnique, we won't need the result column of our data frame.

```{r}
library(caret)
# Selecting every column except result (last one)
initial.predictors = train[,-1]

# Select and drop nearZerVar predictors
dropping.cols = nearZeroVar(initial.predictors)

filtered.predictors = initial.predictors[,-dropping.cols]
```

## Running Principal Component Analysis
This step consists in calling the function _prcomp_, parsing our filtered predictors and selecting the options to scale and center automatically our data.

```{r, eval=FALSE}
# Applying PCA to train data
pca_result = prcomp(filtered.predictors, scale = TRUE, center = TRUE)

pca_result$x = - pca_result$x
pca_result$rotation = - pca_result$rotation
# Saving results
saveRDS(pca_result, "PCA.rds")
```

For saving execution time, that result was stored in _PCA.rds_, and as the rest of those files, are read easy and fast this way:
```{r}
pca_result = readRDS("PCA.rds")
```


Once obtained our PCA, we can visualize the data, to determine mainly the number of minimum component that we will need to obtain a good result (getting higher than 85% of Variance Explained).
```{r}
VE = pca_result$sdev^2
PVE = VE / sum(VE)

# Visualizing the accumulate PVE with the 85% mark and the value of PVE added by each component
par(mfrow=c(1,2)) 
plot(cumsum(PVE), type = "l") +
  abline(h = 0.85, col = "red") +
  abline(v = 40, col = "red")

plot(PVE, type = "b") +
  abline(h = 0.005, col = "red") +
  abline(v = 40, col = "red") 
```
Our theorical mark (85%) is obtainede using around 40 Principal Components. Also, besides this number of PC, each extra one gives us less than 0.5%.

This is why we can choose 60 PC as a good-looking value, having about 91%.

```{r}
cumsum(PVE)[60]
# Setting number of PCAs
n_pcas = 60
```


## Data preparation for model execution
As we are using PCA, the dataset that we are passing to the caret method must contain the data transformed (with 60 cols, the number of predictors calculated before) and the result vector.
For test set, before creating this dataframe, we have to apply the PC values.

```{r}
pca.train.data = data.frame(train.result, pca_result$x[,1:n_pcas])

test.cols = test[, -1]
test.cols = test.cols[, -dropping.cols]
# Calculate PCA values and select 60 
pca.test.predictors = predict(pca_result, test.cols)
pca.test.predictors = pca.test.predictors[, 1:n_pcas]
```



# Classification Algorithms
For training and validating, we define 2 types of cross-validation:
```{r}
# Training options
tuneControl = trainControl(method = "cv",
                           number = 2,
                           verbose = TRUE)


fitControl = trainControl(method = "repeatedcv",
                          number = 5,
                          repeats = 5,
                          verbose = TRUE)
```

First is a simple 2 folds cross-validation (no repetitions). It will be used to aproximate the value of the parameters of the algorithm.

Once we get the appropiate values, we will validate the data, generating our final model using a 5 folds cross-validation with 5 repetitions. 


## Lineal Discriminant Analysis
LDA is a fast and simple classification algorithm that consists in finding a vector where the data proyection gets crearly distinguishable, converting our d x K dimensions space (being d the number of predictors and K the number of classes to determine) to a K dimensions one. This space determines the classes and the individuals that belongs to them.

As with every other algorithm, the first step is to check the hyperparameters, which defines the nature of the algorithm and makes possible to tune it.
```{r}
ldaInfo = getModelInfo("lda")$lda
ldaInfo$parameters
```

This method doesn't have any hyperparameters, so directly we will evaluate the model with all the training dataset (PCA dataset obtained in this case).

### Building the model

```{r, eval=FALSE}
lda.pca.model = train(as.factor(train.result) ~ ., 
                      data = pca.train.data, 
                      method="lda",
                      trControl = fitControl)

# First time, we would execute the training and save the created model for future executions
saveRDS(lda.pca.model, "pca-lda-model.rds")
```

```{r}
# Reading the model saved before...
lda.pca.model = readRDS("pca-lda-model.rds")
lda.pca.model
```

Our validation accuracy obtained is 86.07%, what indicates that this algorithm may work pretty good with new data. Let's find out. 

### Testing final model
To test the model, we pass the test dataset to the model we just obtained.
```{r}
lda.pca.pred = predict(lda.pca.model, newdata = pca.test.predictors)
```


One of the most commom ways to evaluate a classifier model is to examinate the confussion matrix. 
```{r}
lda.pca.mat.conf = confusionMatrix(lda.pca.pred, test.result)
lda.pca.mat.conf
```
The accuracy obtained is a bit lower than in validation, but not significantly enough to conclude that the model was overfitted or were other problems.



## Random Forests
Random Forests is one of the most popular algorithms. Based on Decission Trees, what it does is to create some trees (ntree) for each individual, obtaining a concrete class. Each tree involves a "vote" for this class, and the individual will be classified in the one that gets a higher number of "votes". The algorithm will learn progressively, evaluating more than once the individuals, obtaining good results for many types of problems. 

As mentioned before, we must see the state of the hyperparameters.

```{r}
rfInfo = getModelInfo("rf")$rf
rfInfo$parameters
```

This time, we find the parameter _mtry_. This means, for each tree, the number of predictors used to determine the "vote". As the trees will use different predictors, this could be used also to see which ones are more important for the model.

Let's see the default grid for the parameter _mtry_:

```{r}
rfInfo$grid
```

By default, caret selects a vector of length 3, consisting on 2, the number of total predictors and the half of it. In our case, the 60 predictors are big enough to need to use more values to see the evolution of model based on the value of the parameter. 

Given the complexity of the algorithm, in the first phase of adjustment of the model, we will use a sample of the 10% of the training dataset.

### Building the model
```{r, eval=FALSE}
# 10 per cent of training rows
rows = floor(0.1 * nrow(pca.train.data))
rows.rf = sample(seq_len(nrow(pca.train.data)), size = rows)

# Selecting the sample
data.train.rf = as.data.frame(pca.train.data[rows.rf, ])

# Saving the new result objective
results.rf = as.factor(data.train.rf[,1])

# Rename the first column
names(data.train.rf)[1] = "results"

rf.pca.tune = expand.grid(mtry = seq(1, 20, 1))

# Usamos 100 arboles
rf.pca.model = train(as.factor(results) ~ ., 
                     data = data.train.rf,
                     method = "rf",
                     trControl = tuneControl,
                     ntree = 500, # default
                     tuneGrid = rf.pca.tune)

saveRDS(rf.pca.model, "pca-rf-adjustying-model.rds")
```

As always, we had our model saved so it's better to read directly.
```{r}
rf.pca.model = readRDS("pca-rf-adjusting-model.rds")
plot(rf.pca.model)
```

We get the best accuracy result when _mtry_ = 4, with an 89.4%, so let's train the model. 
```{r, eval=FALSE}
rf.pca.tune = expand.grid(mtry = 4) 

rf.pca.model = train(as.factor(train.result) ~ ., 
                     data = pca.train.data,
                     method = "rf",
                     trControl = fitControl,
                     ntree = 500, # default
                     tuneGrid = rf.pca.tune)

saveRDS(rf.pca.model, "pca-rf-fitted-model.rds")
```


```{r}
rf.pca.model = readRDS("pca-rf-fitted-model.rds")
rf.pca.model
```


### Testing model
Finally, testing the model.
```{r}
rf.pca.pred = predict(rf.pca.model, newdata = pca.test.predictors)

# Creamos la matriz de confusion
rf.pca.mat.conf = confusionMatrix(rf.pca.pred, test.result)

# Tenemos un 95.1% de accuracy
rf.pca.mat.conf$overall
# Visualizamos la matriz de confusion
rf.pca.mat.conf
```


## Multi Layer Perceptron
Finally, other of the most popular algorithms is Multi Layer Perceptron. It is an Artificial Neural Network, having multiple layers by each perceptron. This is the fundamental difference with the classic NN, and makes very interesting this algorithm, because this will let it solve problems no linearly separable.

Let's check the hiperparameters:
```{r}
mlpInfo = getModelInfo("mlp")$mlp
mlpInfo$parameters
```

The only parameter is _size_. It represents the number of hidden layers: number if layers between input and output one for each perceptron.

```{r}
mlpInfo$grid
```
By default, caret uses 1, 3 and 5 to grid this parameter. To evaluate the effect of this parameter, we will use a grid consisting on a vector of numbers from 1 to 20.

### Building the model

```{r, eval = FALSE}
# Define the first tune
mlp.pca.tune = expand.grid(size = seq(1, 20, 1))

mlp.pca.model = train(as.factor(train.result) ~ ., 
                      data = pca.train.data,
                      method = "mlp",
                      trControl = tuneControl,
                      tuneGrid = mlp.pca.tune)

saveRDS(mlp.model, "pca-mlp-adjusting-model.rds")
```

```{r}
mlp.pca.model = readRDS("pca-mlp-adjusting-model.rds")
mlp.pca.model
```

We can observe that the best result was obtained with _size_ = 20 (89.6% accuracy and 88.4% kappa). Now let's train the final model with this configuration.

```{r, eval=FALSE}
mlp.pca.tune = expand.grid(size = 20)
mlp.pca.model = train(as.factor(train.result) ~ ., 
                      data = pca.train.data,
                      method="mlp",
                      trControl = fitControl,
                      tuneGrid = mlp.pca.tune)

saveRDS(mlp.pca.model, "pca-mlp-fitted-model.rds")
```

```{r}
mlp.pca.model = readRDS("pca-mlp-fitted-model.rds")
mlp.pca.model
```
We got 90.0% accuracy on validation.
 
### Testing model

```{r}
mlp.pca.pred = predict(mlp.pca.model, newdata = pca.test.predictors)

mlp.pca.mat.conf = confusionMatrix(mlp.pca.pred, test.result)

mlp.pca.mat.conf
```

Finally, we obtained a slightly lower result (89.3%), so it's pretty good.

